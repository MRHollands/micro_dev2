SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Switch_Assign_Agents_SP] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


/* Add any Assigneds that don't exist in the Table already */
INSERT INTO ASG_Switch ( FIT_ID, Assigned_Agent )
SELECT Switch_Assign_Agents.FIT_ID, Switch_Assign_Agents.Assigned_to
FROM Switch_Assign_Agents LEFT JOIN ASG_Switch ON Switch_Assign_Agents.FIT_ID = ASG_Switch.FIT_ID
WHERE ASG_Switch.FIT_ID IS NULL;

/*Update the Assignee if the FIT is already in the Switch table */
UPDATE ASG_Switch 
SET ASG_Switch.Assigned_Agent = Switch_Assign_Agents.Assigned_to
FROM ASG_Switch
INNER JOIN Switch_Assign_Agents ON Switch_Assign_Agents.FIT_ID = ASG_Switch.FIT_ID 



/* Adds the Move In, if it Exists */
UPDATE CONTRACT 
SET CONTRACT.Move_In = Switch_Assign_Agents.Move_In
From Switch_Assign_Agents
Inner Join INSTALLATION on Switch_Assign_Agents.FIT_ID = INSTALLATION.FiT_ID_Initial 
Inner Join Contract on CONTRACT.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID
WHERE 
Switch_Assign_Agents.Move_In is not null 
AND
CONTRACT.Account_Status='ASG Switch Pending'


END

GO
GRANT EXECUTE ON  [dbo].[Switch_Assign_Agents_SP] TO [admin_role]
GO
GRANT EXECUTE ON  [dbo].[Switch_Assign_Agents_SP] TO [userRole]
GO
