SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author, Matthew Hollands>
-- Create date: <Create Date, 07/03/2017>
-- Description:	<Description,,Procedure that's run by the Create Accounts button on the Switch Upload Accounts form in the Microtricity Database.>
-- =============================================
CREATE PROCEDURE [dbo].[Switch_Account_Upload_SP]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
/*  First move any FITs that are trying to be uploaded but which already exist into an exceptions table. 
	Also removes any records where the Fit_id_Initial field doesn't start with FIT */
insert into Switch_Account_Upload_Exceptions
select Switch_Account_Upload.* from installation inner join Switch_Account_Upload on installation.FiT_ID_Initial = Switch_Account_Upload.fit_id_initial

insert into Switch_Account_Upload_Exceptions
select * from Switch_Account_Upload Where Left(fit_id_initial,3) <>'FIT'

insert into Switch_Account_Upload_Exceptions
select * from Switch_Account_Upload Where Len(fit_id_initial) <>13

insert into Switch_Account_Upload_Exceptions
select * from Switch_Account_Upload Where Right(fit_id_initial,2) <>'-1'


delete Switch_Account_Upload from installation inner join Switch_Account_Upload on installation.FiT_ID_Initial = Switch_Account_Upload.fit_id_initial
Delete Switch_Account_Upload WHERE Left(fit_id_initial,3) <>'FIT'


/* Second - Create the Installation Records */

insert into INSTALLATION
(fit_id_initial,Type_of_Installation,Generation_Type,Export_Status,Generation_Meter_Make_and_Model,Generation_Meter_Model,Remotely_Read_Enabled,Address_1,	
Address_2,City,Postal_Code,Initial_System_Export_Rate,Switch_ID)
select 
fit_id_initial,Type_of_Installation,Generation_Type,Export_Status,Generation_Meter_Make_and_Model,Generation_Meter_Model,Remotely_Read_Enabled,Address_1,	
Address_2,City,Postal_Code,Initial_System_Export_Rate, Switch_ID
from Switch_Account_Upload

/* Third - Update Switch_Account_Upload table with Installation ID's */

update Switch_Account_Upload
set installation_id_link = installation.INSTALLATION_ID
from installation inner join Switch_Account_Upload on installation.FiT_ID_Initial = Switch_Account_Upload.fit_id_initial

/* Fourth - Upload the Contract records */
insert into CONTRACT
(BP_ID_link,
Third_Party,
Third_Party_Ref,
Terms_and_Conditions_Sent_Out_Date,
Terms_and_Conditions_Agreed_Date,
Account_Status,
--VAT_Invoice_Self_Billing,  /*Uncomment this in live, is broken in test */
installation_id_link,
Move_Out,
Proof_of_Ownership_Received,
Payment_Method,
VAT_Inv_Rqd)
select
11174,
Third_Party,
Third_Party_Ref,
Terms_and_Conditions_Sent_Date,
Terms_and_Conditions_Agreed_Date,
Account_Status,
--VAT_Invoice_Self_Billing,
installation_id_link,
Move_Out,
Proof_of_Ownership_Received,
Payment_Method,
VAT_Inv_Rqd
from Switch_Account_Upload

END



GO
GRANT EXECUTE ON  [dbo].[Switch_Account_Upload_SP] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[Switch_Account_Upload_SP] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[Switch_Account_Upload_SP] TO [admin_role]
GO
GRANT EXECUTE ON  [dbo].[Switch_Account_Upload_SP] TO [userRole]
GO
