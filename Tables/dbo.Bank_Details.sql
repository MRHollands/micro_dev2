CREATE TABLE [dbo].[Bank_Details]
(
[Third_Party] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Bank_Acc_Name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Sort_Code] [nchar] (6) COLLATE Latin1_General_CI_AS NOT NULL,
[Bank_Acc_Number] [nchar] (8) COLLATE Latin1_General_CI_AS NOT NULL,
[Company] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[Bank_Details] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Bank_Details] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[Bank_Details] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[Bank_Details] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[Bank_Details] TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING ON  [dbo].[Bank_Details] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bank_Details] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Bank_Details] TO [userRole]
GO
GRANT SELECT ON  [dbo].[Bank_Details] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[Bank_Details] TO [userRole]
GO
