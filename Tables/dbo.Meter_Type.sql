CREATE TABLE [dbo].[Meter_Type]
(
[Man_Key] [tinyint] NULL,
[Manufacturer] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Type] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[Meter_Type] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Meter_Type] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[Meter_Type] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[Meter_Type] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[Meter_Type] TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING ON  [dbo].[Meter_Type] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[Meter_Type] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Meter_Type] TO [userRole]
GO
GRANT SELECT ON  [dbo].[Meter_Type] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[Meter_Type] TO [userRole]
GO
