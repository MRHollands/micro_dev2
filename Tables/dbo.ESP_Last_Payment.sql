CREATE TABLE [dbo].[ESP_Last_Payment]
(
[SAP_CA] [float] NULL,
[Last_Payment_Date] [datetime] NULL,
[Last_Payment_Amount] [float] NULL
) ON [PRIMARY]
GO
