CREATE TABLE [dbo].[Attachments]
(
[Attch_ID] [int] NOT NULL IDENTITY(1, 1),
[Contract_ID] [int] NULL,
[Title] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Document] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SSMA_TimeStamp] [timestamp] NOT NULL,
[Modified_By] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Date_modified] [datetime] NULL,
[Time_modified] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Attachments] ADD CONSTRAINT [Attachments$PrimaryKey] PRIMARY KEY CLUSTERED  ([Attch_ID]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[Attachments] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Attachments] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[Attachments] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[Attachments] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[Attachments] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[Attachments] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Attachments] TO [userRole]
GO
GRANT SELECT ON  [dbo].[Attachments] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[Attachments] TO [userRole]
GO
