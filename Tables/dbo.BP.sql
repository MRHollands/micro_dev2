CREATE TABLE [dbo].[BP]
(
[BP_ID] [int] NOT NULL IDENTITY(1, 1),
[GeneratorID] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Customer_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Company] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Title] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Last_Name] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[First_Name] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[VAT_Reg_Number] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Email_Address] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Email_2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Email_3] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Business_Phone] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Home_Phone] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Mobile_Phone] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Fax_Number] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Paperless_Customer] [bit] NULL CONSTRAINT [DF__BP__Paperless_Cu__27C3E46E] DEFAULT ((0)),
[Contact_Method_Email] [bit] NULL CONSTRAINT [DF__BP__Contact_Meth__28B808A7] DEFAULT ((0)),
[Contact_Method_Letter] [bit] NULL CONSTRAINT [DF__BP__Contact_Meth__29AC2CE0] DEFAULT ((0)),
[Address_1] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Address_2] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[City] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[County] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Postal_Code] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Added_To_SAP] [bit] NULL CONSTRAINT [DF__BP__Added_To_SAP__2AA05119] DEFAULT ((0)),
[ID_Received] [bit] NULL CONSTRAINT [DF__BP__ID_Received__2B947552] DEFAULT ((0)),
[Modified_By] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Date_Modified] [datetime] NULL,
[Time_Modified] [datetime] NULL,
[Created_By] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Created_Date] [date] NULL CONSTRAINT [DF__BP__Created_Date__4DE98D56] DEFAULT (getdate()),
[Created_Time] [time] NULL CONSTRAINT [DF__BP__Created_Time__4EDDB18F] DEFAULT (CONVERT([time],getdate(),(0))),
[Modified_By2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__BP__modified_by2__63D8CE75] DEFAULT (suser_name()),
[Created_By2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__BP__created_by2__64CCF2AE] DEFAULT (suser_name()),
[Created_Date2] [datetime] NULL CONSTRAINT [DF__BP__created_date__65C116E7] DEFAULT (getdate()),
[Modified_Date2] [datetime] NULL CONSTRAINT [DF__BP__modified_dat__66B53B20] DEFAULT (getdate()),
[SSMA_TimeStamp] [timestamp] NOT NULL,
[Meter_Read_Assistance_Required] [bit] NULL,
[Vendor_Number] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create trigger [dbo].[bp_trg] on [dbo].[BP]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE BP SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM BP f INNER JOIN Inserted i  on i.bp_id = f.bp_id;
  END;
GO
ALTER TABLE [dbo].[BP] ADD CONSTRAINT [BP$PrimaryKey] PRIMARY KEY CLUSTERED  ([BP_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_BP_29_439672614__K1_2_4_5_6_7_9_19_20_21_22_23] ON [dbo].[BP] ([BP_ID]) INCLUDE ([Address_1], [Address_2], [City], [Company], [County], [Email_Address], [First_Name], [GeneratorID], [Last_Name], [Postal_Code], [Title]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_BP_29_439672614__K1_2_3_4_5_6_7_8_9_10_11_12_13_14_16_19_20_21_22_23_24_25_38_39] ON [dbo].[BP] ([BP_ID]) INCLUDE ([Added_To_SAP], [Address_1], [Address_2], [Business_Phone], [City], [Company], [County], [Customer_Type], [Email_2], [Email_3], [Email_Address], [First_Name], [GeneratorID], [Home_Phone], [ID_Received], [Last_Name], [Meter_Read_Assistance_Required], [Mobile_Phone], [Paperless_Customer], [Postal_Code], [Title], [VAT_Reg_Number], [Vendor_Number]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [BP$Generator_ID] ON [dbo].[BP] ([GeneratorID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [BP$Postal_Code] ON [dbo].[BP] ([Postal_Code]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[BP] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[BP] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[BP] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[BP] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[BP] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[BP] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[BP] TO [userRole]
GO
GRANT SELECT ON  [dbo].[BP] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[BP] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP]', 'SCHEMA', N'dbo', 'TABLE', N'BP', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Added_To_SAP]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Added_To_SAP'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Address_1]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Address_1'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Address_2]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Address_2'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[BP_ID]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'BP_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Business_Phone]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Business_Phone'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[City]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'City'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Company]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Company'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Contact_Method_Email]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Contact_Method_Email'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Contact_Method_Letter]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Contact_Method_Letter'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[County]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'County'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Customer_Type]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Customer_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Date_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Date_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Email_Address]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Email_Address'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Fax_Number]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Fax_Number'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Last_Name]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'First_Name'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[GeneratorID]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'GeneratorID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Home_Phone]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Home_Phone'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[ID_Received]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'ID_Received'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[First_Name]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Last_Name'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Mobile_Phone]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Mobile_Phone'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Modified_By]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Modified_By'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Paperless_Customer]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Paperless_Customer'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Postal_Code]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Postal_Code'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Time_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Time_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Title]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'Title'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[VAT_Reg_Number]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'COLUMN', N'VAT_Reg_Number'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'CONSTRAINT', N'BP$PrimaryKey'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Generator_ID]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'INDEX', N'BP$Generator_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System.[BP].[Postal_Code]', 'SCHEMA', N'dbo', 'TABLE', N'BP', 'INDEX', N'BP$Postal_Code'
GO
