CREATE TABLE [dbo].[MASS_READ_UPLOAD_TEMPLATE]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[FiT_ID] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[INSTALLATION_ID_link] [int] NULL,
[Generation_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Generation_Capacity] [int] NULL,
[Last_Read_Date] [datetime] NULL,
[New_Read_Date] [datetime] NULL,
[New_Read_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[New_Read_Reason] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Last_Generation_Read] [int] NULL,
[New_Generation_Read] [int] NULL,
[Total_Gen_Read_Diff] [int] NULL,
[Generation_Read_Valid] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Last_Export_Read] [int] NULL,
[New_Export_Read] [int] NULL,
[Total_Exp_Read_Diff] [int] NULL,
[Export_Read_Valid] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[UPLOAD_TO_READ_TABLE] [bit] NULL CONSTRAINT [DF__MASS_READ__UPLOA__2759D01A] DEFAULT ((0)),
[HrsPerDay] [decimal] (5, 2) NULL,
[SSMA_TimeStamp] [timestamp] NOT NULL,
[Account_Status] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MASS_READ_UPLOAD_TEMPLATE] ADD CONSTRAINT [MASS_READ_UPLOAD_TEMPLATE$PrimaryKey] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[MASS_READ_UPLOAD_TEMPLATE] TO [userRole]
GO
GRANT INSERT ON  [dbo].[MASS_READ_UPLOAD_TEMPLATE] TO [userRole]
GO
GRANT SELECT ON  [dbo].[MASS_READ_UPLOAD_TEMPLATE] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[MASS_READ_UPLOAD_TEMPLATE] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Export_Read_Valid]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Export_Read_Valid'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[FiT_ID]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'FiT_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Generation_Capacity]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Generation_Capacity'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Generation_Read_Valid]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Generation_Read_Valid'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Generation_Type]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Generation_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[ID]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[INSTALLATION_ID_link]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'INSTALLATION_ID_link'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Last_Export_Read]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Last_Export_Read'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Last_Generation_Read]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Last_Generation_Read'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Last_Read_Date]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Last_Read_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[New_Export_Read]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'New_Export_Read'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[New_Generation_Read]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'New_Generation_Read'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[New_Read_Date]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'New_Read_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[New_Read_Reason]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'New_Read_Reason'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[New_Read_Type]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'New_Read_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Total_Exp_Read_Diff]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Total_Exp_Read_Diff'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[Total_Gen_Read_Diff]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'Total_Gen_Read_Diff'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[UPLOAD_TO_READ_TABLE]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'COLUMN', N'UPLOAD_TO_READ_TABLE'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[MASS_READ_UPLOAD_TEMPLATE].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'MASS_READ_UPLOAD_TEMPLATE', 'CONSTRAINT', N'MASS_READ_UPLOAD_TEMPLATE$PrimaryKey'
GO
