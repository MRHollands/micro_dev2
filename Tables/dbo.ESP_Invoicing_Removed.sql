CREATE TABLE [dbo].[ESP_Invoicing_Removed]
(
[Contract_ID_Link] [int] NOT NULL,
[Invoice_Number] [int] NOT NULL,
[Contract_Account] [int] NOT NULL,
[First_Name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Last_Name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Email_Address] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Address] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Address_2] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Address_3] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[City] [nvarchar] (150) COLLATE Latin1_General_CI_AS NULL,
[County] [nvarchar] (150) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [nvarchar] (12) COLLATE Latin1_General_CI_AS NULL,
[Contract_Start_Date] [date] NULL,
[Invoice_Date] [date] NULL,
[Invoice_Due_Date] [date] NULL,
[Invoice_Method] [nvarchar] (15) COLLATE Latin1_General_CI_AS NULL,
[Total_Past_Due_Charges] [money] NULL,
[Last_Payment_Date] [date] NULL,
[Last_Payment_Amount] [money] NULL,
[Billing_Start] [date] NULL,
[Billing_End] [date] NULL,
[Total_Anticipated_Output] [numeric] (12, 2) NULL,
[MPAN] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Total_Amount_Due] [money] NULL,
[Gross_Price] [money] NULL,
[Start_Read] [int] NULL,
[End_Read] [int] NULL,
[Generation_kWh] [numeric] (12, 2) NULL,
[Fit_Gen_Price] [money] NULL,
[Fit_Gen_Cost] [money] NULL,
[Fit_Exp_Price] [money] NULL,
[Fit_Exp_Cost] [money] NULL,
[Export_kWh] [numeric] (12, 2) NULL,
[Fit_Discount] [money] NULL,
[Net_Charges] [money] NULL,
[Discount] [money] NULL,
[Net_Cost] [money] NULL,
[VAT] [money] NULL,
[Gross_Cost] [money] NULL,
[Late_Fees] [money] NULL,
[Total_Due] [money] NULL,
[Bank_Account_Number] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Invoice_Sent_Date] [date] NULL,
[meter_Read_Reason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Payment_Method] [nvarchar] (5) COLLATE Latin1_General_CI_AS NULL,
[System_Size] [float] NULL,
[Meter_ID] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Cancellation_Date] [date] NULL,
[Total_Electricity_Charge_inc_VAT] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ESP_Invoicing_Removed] TO [userRole]
GO
