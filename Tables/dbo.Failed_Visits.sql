CREATE TABLE [dbo].[Failed_Visits]
(
[Installation_ID] [int] NOT NULL,
[Read_ID] [int] NOT NULL IDENTITY(1, 1),
[Visit_Date] [datetime] NULL,
[MSN] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Request_Status] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Visit_Number] [int] NULL,
[Notes_A] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Notes_B] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Created_By] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__Failed_Vi__Creat__5E1FF51F] DEFAULT (suser_name()),
[Created_Date] [datetime] NOT NULL CONSTRAINT [DF__Failed_Vi__Creat__5F141958] DEFAULT (getdate()),
[Modified_By] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__Failed_Vi__Modif__60083D91] DEFAULT (suser_name()),
[Modified_Date] [datetime] NOT NULL CONSTRAINT [DF__Failed_Vi__Modif__60FC61CA] DEFAULT (getdate()),
[Status] [varchar] (255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create trigger [dbo].[failed_visit_trg] on [dbo].[Failed_Visits]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE failed_visits SET modified_date = GETDATE() , modified_by = SUSER_NAME()
	FROM failed_visits f INNER JOIN Inserted i  on i.read_id = f.read_id;
  END;
  
GO
ALTER TABLE [dbo].[Failed_Visits] ADD CONSTRAINT [Failed_visits$PrimaryKey] PRIMARY KEY CLUSTERED  ([Read_ID]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[Failed_Visits] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Failed_Visits] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[Failed_Visits] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[Failed_Visits] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[Failed_Visits] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Failed_Visits] TO [userRole]
GO
GRANT SELECT ON  [dbo].[Failed_Visits] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[Failed_Visits] TO [userRole]
GO
