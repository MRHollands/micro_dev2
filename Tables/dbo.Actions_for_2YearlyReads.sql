CREATE TABLE [dbo].[Actions_for_2YearlyReads]
(
[record_id] [int] NOT NULL IDENTITY(1, 1),
[Installation_ID] [int] NOT NULL,
[Date_Note_Made] [datetime] NULL CONSTRAINT [DF__Actions_f__Date___08162EEB] DEFAULT (getdate()),
[Action] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Created_By] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__Actions_f__Creat__090A5324] DEFAULT (suser_name()),
[Status] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Date_Visit_Booked] [datetime] NULL,
[Modified_date] [datetime] NULL CONSTRAINT [DF__Actions_f__Modif__09FE775D] DEFAULT (getdate()),
[Modified_by] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__Actions_f__Modif__0AF29B96] DEFAULT (suser_name())
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 create trigger [dbo].[Actions_for_2YearlyReads_trg] on [dbo].[Actions_for_2YearlyReads]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE Actions_for_2YearlyReads SET modified_date = GETDATE() , modified_by = SUSER_NAME()
	FROM Actions_for_2YearlyReads f INNER JOIN Inserted i  on i.record_id = f.record_id;
  END;
GO
ALTER TABLE [dbo].[Actions_for_2YearlyReads] ADD CONSTRAINT [Actions_for_2YearlyReads_pk] PRIMARY KEY CLUSTERED  ([record_id]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[Actions_for_2YearlyReads] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Actions_for_2YearlyReads] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[Actions_for_2YearlyReads] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[Actions_for_2YearlyReads] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[Actions_for_2YearlyReads] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Actions_for_2YearlyReads] TO [userRole]
GO
GRANT SELECT ON  [dbo].[Actions_for_2YearlyReads] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[Actions_for_2YearlyReads] TO [userRole]
GO
