CREATE TABLE [dbo].[Data]
(
[ID] [float] NULL,
[Meter_Read_Date(NEW)] [datetime] NULL,
[Extension_1_Export_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_2_Export_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_1_Tariff_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Extension_2_Tariff_Rate] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
