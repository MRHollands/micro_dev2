CREATE TABLE [dbo].[READINGS]
(
[READ_ID] [int] NOT NULL IDENTITY(1, 1),
[INSTALLATION_ID_link] [int] NULL,
[Meter_Read_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Date] [datetime] NULL,
[Meter_Read_Reason] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Generation_Read] [int] NULL,
[Export_Read] [int] NULL,
[IncludedInLevelisation] [bit] NULL CONSTRAINT [DF__READINGS__Includ__1881A0DE] DEFAULT ((0)),
[Gen_Validation] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Modified_By] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Date_Modified] [datetime] NULL,
[Time_Modified] [datetime] NULL,
[SSMA_TimeStamp] [timestamp] NOT NULL,
[Claimed] [bit] NULL,
[Generation_Clocked] [int] NULL,
[Export_Clocked] [int] NULL,
[Notes] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[MSN] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Modified_by2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__READINGS__Modifi__731B1205] DEFAULT (suser_name()),
[Created_by2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__READINGS__Create__740F363E] DEFAULT (suser_name()),
[Created_date2] [datetime] NULL CONSTRAINT [DF__READINGS__Create__75035A77] DEFAULT (getdate()),
[Modified_date2] [datetime] NULL CONSTRAINT [DF__READINGS__Modifi__75F77EB0] DEFAULT (getdate()),
[ESP_Billed] [bit] NULL,
[Recieved_Method] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Photo] [bit] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create trigger [dbo].[readings_trg] on [dbo].[READINGS]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE readings SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM readings f INNER JOIN Inserted i  on i.read_id = f.read_id;
  END;
GO
ALTER TABLE [dbo].[READINGS] ADD CONSTRAINT [READINGS$PrimaryKey] PRIMARY KEY CLUSTERED  ([READ_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [READINGS$FiT_ID] ON [dbo].[READINGS] ([INSTALLATION_ID_link]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[READINGS] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[READINGS] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[READINGS] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[READINGS] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[READINGS] TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING ON  [dbo].[READINGS] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[READINGS] TO [admin_role]
GO
GRANT DELETE ON  [dbo].[READINGS] TO [userRole]
GO
GRANT INSERT ON  [dbo].[READINGS] TO [userRole]
GO
GRANT SELECT ON  [dbo].[READINGS] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[READINGS] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Date_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Date_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Export_Read]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Export_Read'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Gen_Validation]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Gen_Validation'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Generation_Read]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Generation_Read'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[IncludedInLevelisation]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'IncludedInLevelisation'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[INSTALLATION_ID_link]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'INSTALLATION_ID_link'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Meter_Read_Date]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Meter_Read_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Meter_Read_Reason]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Meter_Read_Reason'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Meter_Read_Type]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Meter_Read_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Modified_By]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Modified_By'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[READ_ID]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'READ_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[Time_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'COLUMN', N'Time_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'CONSTRAINT', N'READINGS$PrimaryKey'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READINGS].[FiT_ID]', 'SCHEMA', N'dbo', 'TABLE', N'READINGS', 'INDEX', N'READINGS$FiT_ID'
GO
