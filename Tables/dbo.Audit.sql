CREATE TABLE [dbo].[Audit]
(
[tablename1] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[modified_date] [datetime] NOT NULL,
[username] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[primaryCol] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PrimaryValue] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[id] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Audit] ADD CONSTRAINT [audit_Pk] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
GRANT INSERT ON  [dbo].[Audit] TO [userRole]
GO
GRANT SELECT ON  [dbo].[Audit] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[Audit] TO [userRole]
GO
