CREATE TABLE [dbo].[USERS]
(
[USER_ID] [int] NOT NULL IDENTITY(1, 1),
[USERNAME] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[FirstName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Team] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Position] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Loginid] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[USERS] ADD CONSTRAINT [USERS$PrimaryKey] PRIMARY KEY CLUSTERED  ([USER_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [USERS$USER_ID] ON [dbo].[USERS] ([USER_ID]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[USERS] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[USERS] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[USERS] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[USERS] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[USERS] TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING ON  [dbo].[USERS] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[USERS] TO [admin_role]
GO
GRANT DELETE ON  [dbo].[USERS] TO [ECO\Luke.Barton]
GO
GRANT INSERT ON  [dbo].[USERS] TO [ECO\Luke.Barton]
GO
GRANT SELECT ON  [dbo].[USERS] TO [ECO\Luke.Barton]
GO
GRANT UPDATE ON  [dbo].[USERS] TO [ECO\Luke.Barton]
GO
GRANT INSERT ON  [dbo].[USERS] TO [userRole]
GO
GRANT SELECT ON  [dbo].[USERS] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[USERS] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[USERS]', 'SCHEMA', N'dbo', 'TABLE', N'USERS', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[USERS].[USER_ID]', 'SCHEMA', N'dbo', 'TABLE', N'USERS', 'COLUMN', N'USER_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[USERS].[USERNAME]', 'SCHEMA', N'dbo', 'TABLE', N'USERS', 'COLUMN', N'USERNAME'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[USERS].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'USERS', 'CONSTRAINT', N'USERS$PrimaryKey'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[USERS].[USER_ID]', 'SCHEMA', N'dbo', 'TABLE', N'USERS', 'INDEX', N'USERS$USER_ID'
GO
