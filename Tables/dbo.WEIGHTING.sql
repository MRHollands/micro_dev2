CREATE TABLE [dbo].[WEIGHTING]
(
[WEIGHTING_ID] [int] NOT NULL IDENTITY(1, 1),
[Usage_Date] [datetime] NULL,
[Generation_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[WeightingMonth] [money] NULL,
[Days_In_Month] [int] NULL,
[WeightingDay] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WEIGHTING] ADD CONSTRAINT [WEIGHTING$PrimaryKey] PRIMARY KEY CLUSTERED  ([WEIGHTING_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [WEIGHTING$ID] ON [dbo].[WEIGHTING] ([WEIGHTING_ID]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[WEIGHTING] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[WEIGHTING] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[WEIGHTING] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[WEIGHTING] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[WEIGHTING] TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING ON  [dbo].[WEIGHTING] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[WEIGHTING] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[WEIGHTING] TO [userRole]
GO
GRANT SELECT ON  [dbo].[WEIGHTING] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[WEIGHTING] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING].[Days_In_Month]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', 'COLUMN', N'Days_In_Month'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING].[Generation_Type]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', 'COLUMN', N'Generation_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING].[Usage_Date]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', 'COLUMN', N'Usage_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING].[WEIGHTING_ID]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', 'COLUMN', N'WEIGHTING_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING].[WeightingDay]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', 'COLUMN', N'WeightingDay'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING].[WeightingMonth]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', 'COLUMN', N'WeightingMonth'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', 'CONSTRAINT', N'WEIGHTING$PrimaryKey'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[WEIGHTING].[ID]', 'SCHEMA', N'dbo', 'TABLE', N'WEIGHTING', 'INDEX', N'WEIGHTING$ID'
GO
