CREATE TABLE [dbo].[SALES_LEDGER]
(
[SL_ID] [int] NOT NULL IDENTITY(1, 1),
[Statement_Number] [int] NULL,
[Statement_Type] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Created_Date] [datetime] NULL,
[Cancelled_Date] [datetime] NULL,
[Canx_ID] [int] NULL,
[CONTRACT_ID_link] [int] NULL,
[Posting_Date] [datetime] NULL,
[Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Payment_Method] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SAP_CA_Paid_To] [int] NULL,
[From_Read] [int] NULL,
[To_Read] [int] NULL,
[Amount] [money] NULL,
[FiT_Period] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Statement_ID] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Related_Document] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Modified_By] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Date_Modified] [datetime] NULL,
[Time_Modified] [datetime] NULL,
[SSMA_TimeStamp] [timestamp] NOT NULL,
[Modified_by2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Created_by2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Created_date2] [datetime] NULL,
[Modified_date2] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SALES_LEDGER] ADD CONSTRAINT [SALES_LEDGER$PrimaryKey] PRIMARY KEY CLUSTERED  ([SL_ID]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[SALES_LEDGER] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[SALES_LEDGER] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[SALES_LEDGER] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[SALES_LEDGER] TO [admin_role]
GO
GRANT DELETE ON  [dbo].[SALES_LEDGER] TO [userRole]
GO
GRANT INSERT ON  [dbo].[SALES_LEDGER] TO [userRole]
GO
GRANT SELECT ON  [dbo].[SALES_LEDGER] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[SALES_LEDGER] TO [userRole]
GO
