CREATE TABLE [dbo].[MASS_COT_EXCEPTIONS]
(
[FIT_ID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Third_Party_Ref] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address_1] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Address_2] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Postcode] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Previous_Reading] [int] NULL,
[New_Reading] [int] NULL,
[New_Reading_Date] [datetime] NULL,
[Old_3rd_Party] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[New_3rd_Party] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Old_Sort_Code] [nchar] (6) COLLATE Latin1_General_CI_AS NOT NULL,
[Old_Bank_Account] [nchar] (8) COLLATE Latin1_General_CI_AS NOT NULL,
[New_Sort_Code] [nchar] (6) COLLATE Latin1_General_CI_AS NOT NULL,
[New_Bank_Account] [nchar] (8) COLLATE Latin1_General_CI_AS NOT NULL,
[Thrd_Prty_Ref_Chk] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[Thrd_Prty_Chk] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[PostCde_Chk] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[Reading_Chk] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[MoveIn_Chk] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[Check_All] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[UPLOAD_TO_COT_TABLE] [bit] NULL,
[Installation_ID_link] [int] NULL
) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[MASS_COT_EXCEPTIONS] TO [userRole]
GO
GRANT INSERT ON  [dbo].[MASS_COT_EXCEPTIONS] TO [userRole]
GO
GRANT SELECT ON  [dbo].[MASS_COT_EXCEPTIONS] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[MASS_COT_EXCEPTIONS] TO [userRole]
GO
