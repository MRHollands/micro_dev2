CREATE TABLE [dbo].[TEMP_Reads]
(
[FiT_ID_Initial] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Type] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Reason] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Meter_Read_Date] [datetime] NULL,
[Generation_Read] [float] NULL,
[Export_Read] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT CONTROL ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT REFERENCES ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT TAKE OWNERSHIP ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[TEMP_Reads] TO [admin_role]
GO
