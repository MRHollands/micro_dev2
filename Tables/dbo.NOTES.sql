CREATE TABLE [dbo].[NOTES]
(
[NOTE_ID] [int] NOT NULL IDENTITY(1, 1),
[CONTRACT_ID_link] [int] NOT NULL,
[Date_Applied] [datetime] NULL,
[Applied_By] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Reason] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Note] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Modified_By] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Date_Modified] [datetime] NULL,
[Time_Modified] [datetime] NULL,
[SSMA_TimeStamp] [timestamp] NOT NULL,
[Modified_by2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__NOTES__Modified___77DFC722] DEFAULT (suser_name()),
[Created_by2] [varchar] (255) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__NOTES__Created_b__78D3EB5B] DEFAULT (suser_name()),
[Created_date2] [datetime] NULL CONSTRAINT [DF__NOTES__Created_d__79C80F94] DEFAULT (getdate()),
[Modified_date2] [datetime] NULL CONSTRAINT [DF__NOTES__Modified___7ABC33CD] DEFAULT (getdate())
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create trigger [dbo].[notes_trg] on [dbo].[NOTES]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE notes SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM notes f INNER JOIN Inserted i  on i.note_id = f.note_id;
  END;
GO
ALTER TABLE [dbo].[NOTES] ADD CONSTRAINT [NOTES$PrimaryKey] PRIMARY KEY CLUSTERED  ([NOTE_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NOTES$CONTRACT_ID] ON [dbo].[NOTES] ([CONTRACT_ID_link]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NOTES$NOTE_ID] ON [dbo].[NOTES] ([NOTE_ID]) ON [PRIMARY]
GO
GRANT DELETE ON  [dbo].[NOTES] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[NOTES] TO [admin_role]
GO
GRANT ALTER ON  [dbo].[NOTES] TO [admin_role]
GO
GRANT SELECT ON  [dbo].[NOTES] TO [admin_role]
GO
GRANT UPDATE ON  [dbo].[NOTES] TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING ON  [dbo].[NOTES] TO [admin_role]
GO
GRANT VIEW DEFINITION ON  [dbo].[NOTES] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[NOTES] TO [userRole]
GO
GRANT SELECT ON  [dbo].[NOTES] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[NOTES] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[Applied_By]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'Applied_By'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[CONTRACT_ID_link]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'CONTRACT_ID_link'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[Date_Applied]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'Date_Applied'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[Date_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'Date_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[Modified_By]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'Modified_By'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[Note]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'Note'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[NOTE_ID]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'NOTE_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[Reason]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'Reason'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[Time_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'COLUMN', N'Time_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'CONSTRAINT', N'NOTES$PrimaryKey'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[CONTRACT_ID]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'INDEX', N'NOTES$CONTRACT_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[NOTES].[NOTE_ID]', 'SCHEMA', N'dbo', 'TABLE', N'NOTES', 'INDEX', N'NOTES$NOTE_ID'
GO
