SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MSG_Audit_Check]
AS
SELECT  A.FiT_ID_Initial, B.Third_Party_Ref, B.Account_Status, C.First_Name, C.Last_Name, A.Supply_MPAN, A.Generation_MSN, A.Generation_Type, A.Postal_Code, A.OS_Grid_Ref, A.MCS_Certification_Number_1, 
        A.Initial_System_Tariff_Rate, A.Initial_System_Export_Rate, A.Initial_System_End_Date, A.Export_Status, B.Move_In, B.Move_Out, B.Proof_of_Ownership_Received, A.Created_by2, 
        B.Third_Party,
		A.Modified_By,
		CONVERT(VARCHAR(10), A.Created_date2, 103) AS Created_Date,

        CASE 
			WHEN LEFT(A.FiT_ID_Initial, 3) <> 'FIT' THEN 'FIT ID Error' 
			WHEN RIGHT(FiT_ID_Initial, 2) <> '-1' THEN 'Fit_ID_Initial Error' 
			WHEN RIGHT(FiT_ID_Ext1, 2)    <> '-2' THEN 'Fit_ID_Ext1 Error' 
			WHEN RIGHT(FiT_ID_Ext2, 2)    <> '-3' THEN 'Fit_ID_Ext2 Error' 
			WHEN RIGHT(FiT_Related, 2)    <> '-4' THEN 'Fit_Related Error' 
			WHEN A.FiT_ID_Initial IS NULL AND B.Account_Status IN ('Registered Live', 'Registered Pending') 
				THEN 'Null Fit not Live or Pending Error' 
			WHEN B.Payment_Method = 'Awaiting contract info' 
				THEN 'Account Status Error' 
			WHEN B.Payment_Method <> 'BACs' THEN 'Payment Method Error' 
			WHEN B.Account_Status
				IN ('Awaiting contract info', 'Closed', 'COT Complete', 'Lost Customer', 'Prospect', 'Prospective Business Customer', 'Prospective Domestic Customer') AND B.Move_Out = '12/31/9999' OR
				B.Account_Status IN ('Registered - COT New Occupier', 'Change of Supply In Progress', 'Account Suspended', 'Registered Live', 'Registered Pending', 'Registration On Hold', 'Suspended-2 Yr Read', 
				'Under Investigation') AND B.Move_Out <> '12/31/9999' 
				THEN 'Move Out Date Error' 
			WHEN LEN(A.Supply_MPAN) <> 13 OR
            (A.Supply_MPAN IS NULL AND B.Account_Status <> 'Awaiting Contract Info') 
				THEN 'Supply MPAN Error' 
			WHEN Export_Status <> 'Deemed Export (<30kW)' THEN 'Export Status Error' 
			WHEN patindex('[A-Z][0-9] [0-9][A-Z][A-Z]', A.Postal_Code) = 0 AND patindex('[A-Z][0-9][0-9] [0-9][A-Z][A-Z]', 
				A.Postal_Code) = 0 AND patindex('[A-Z][A-Z][0-9] [0-9][A-Z][A-Z]', A.Postal_Code) = 0 AND patindex('[A-Z][A-Z][0-9][0-9] [0-9][A-Z][A-Z]', A.Postal_Code) = 0 AND patindex('[A-Z][0-9][A-Z] [0-9][A-Z][A-Z]', A.Postal_Code) 
                = 0 AND patindex('[A-Z][A-Z][0-9][A-Z] [0-9][A-Z][A-Z]', A.Postal_Code) 
                = 0 
				THEN 'Post Code Error' 
			WHEN A.Generation_Type <> 'Solar PV' THEN 'Generation Type Error' 
			WHEN A.Type_of_Installation <> 'Standard' THEN 'Installation Type Error' 
			WHEN A.OS_Grid_Ref IS NOT NULL OR
                         patindex('[A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]', A.OS_Grid_Ref) = 0 THEN 'OS Grid Ref Error' 
			WHEN A.Application_Received_Date IS NULL 
                THEN 'Application Recieved Date Error' 
			WHEN A.Initial_System_Tariff_Rate IS NULL AND B.Account_Status <> 'Awaiting contract info' 
				THEN 'Initial System Tariff Error' 
			WHEN A.Initial_System_Export_Rate IS NULL AND B.Account_Status <> 'Awaiting contract info' 
				THEN 'Export Tariff Error' 
			WHEN Year(A.Initial_System_End_Date) - Year(A.Initial_System_Start_Date) < 20 
				THEN 'Initial System End Date Error' 
			WHEN LEFT(A.MCS_Certification_Number_1, 4) <> 'MCS-' OR patindex('-[A-Z]', RIGHT(A.MCS_Certification_Number_1, 2)) = 0 
				THEN 'MCS Cerification Error' 
			WHEN B.Third_Party_Ref IS NULL 
                THEN 'Third Party Ref is Blank Error' 
			WHEN B.Proof_of_Ownership_Received = 0 AND B.Account_Status <> 'Awaiting contract info' THEN 'Proof of Ownership Error' 
			WHEN LEN(A.Generation_MSN)   > 10 THEN 'Generation MSN longer than 10 Error' 
			WHEN A.Export_MSN IS NOT NULL THEN 'Export MSN is not blank Error' 
			WHEN A.Total_Installed_Capacity_kW IS NULL OR A.Total_Declared_Net_Capacity_kW IS NULL THEN 'TIC or DNC is blank Error' 
			WHEN A.Total_Installed_Capacity_kW > 5 OR A.Total_Declared_Net_Capacity_kW > 5 THEN 'TIC or DNC is greater than 5kW Error' 
			ELSE NULL 
		END AS Check_Error
FROM            dbo.INSTALLATION AS A INNER JOIN
                         dbo.CONTRACT AS B ON A.INSTALLATION_ID = B.INSTALLATION_ID_link INNER JOIN
                         dbo.BP AS C ON B.BP_ID_link = C.BP_ID INNER JOIN
                         dbo.Bank_Details AS D ON B.Third_Party = D.Third_Party
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "A"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "B"
            Begin Extent = 
               Top = 6
               Left = 360
               Bottom = 136
               Right = 652
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 6
               Left = 690
               Bottom = 136
               Right = 903
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "D"
            Begin Extent = 
               Top = 6
               Left = 941
               Bottom = 136
               Right = 1130
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'MSG_Audit_Check', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'MSG_Audit_Check', NULL, NULL
GO
