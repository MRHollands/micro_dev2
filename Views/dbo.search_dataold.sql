SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view [dbo].[search_dataold] as
SELECT BP.BP_ID as BP_ID, (ISNULL(bp.Title,'_') + ' ' + ISNULL(bp.First_Name,'_') + ' ' + ISNULL(bp.Last_Name,'_')) AS Full_Name,
ISNULL(BP.GeneratorID,'_') as GeneratorID, ISNULL(BP.Address_1,'_') as Address_1, ISNULL(BP.Address_2,'_') as Address_2,
ISNULL(BP.City,'_') as City, ISNULL(BP.County,'_') as County, ISNULL(BP.Postal_Code,'_') as Postal_Code, ISNULL(BP.Email_Address,'_') as Email_Address, CT.CONTRACT_ID as CONTRACT_ID, 
ISNULL(CT.Account_Status,'_') as Account_Status, ISNULL(CT.Move_In,'1900-01-01') as Move_In, ISNULL(CT.Move_Out,'1900-01-01') as Move_Out, ISNULL(CT.Third_Party,'_') as Third_Party, ISNULL(CT.SAP_BP,0) as SAP_BP, 
ISNULL(CT.SAP_CA,0) as SAP_CA, ISNULL(i.Supply_MPAN,'_') as Supply_MPAN, i.INSTALLATION_ID as INSTALLATION_ID , ISNULL(i.Generation_Type,'_') as Generation_Type, ISNULL(i.Address_1,'_') as iAddress_1, 
ISNULL(i.Address_2,'_') as iAddress_2, ISNULL(i.City,'_') as iCity,  isnull(i.County,'_') as iCounty,
ISNULL(i.Postal_Code,'_') as iPostal_Code,   ISNULL(i.FiT_ID_Initial,'_') as  FiT_Main,
BP.Title,
BP.First_Name,
BP.Last_Name,
BP.Customer_Type,
BP.VAT_Reg_Number,
BP.Added_To_SAP,
BP.ID_Received,
BP.Business_Phone,
BP.Home_Phone,
BP.Mobile_Phone,
BP.Paperless_Customer,
CT.IMPORTANT,
BP.Company



FROM (CONTRACT CT LEFT JOIN BP ON CT.BP_ID_link = BP.BP_ID) 
LEFT JOIN INSTALLATION i ON CT.INSTALLATION_ID_link = i.INSTALLATION_ID;


GO
GRANT SELECT ON  [dbo].[search_dataold] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[search_dataold] TO [userRole]
GO
GRANT SELECT ON  [dbo].[search_dataold] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[search_dataold] TO [userRole]
GO
