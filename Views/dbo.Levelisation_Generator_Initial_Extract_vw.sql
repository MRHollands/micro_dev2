SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[Levelisation_Generator_Initial_Extract_vw]
AS
    SELECT        CASE WHEN  dbo.Tables_Combined.Generation_Type = 'Hydro' THEN Scheme_Type + '75' ELSE Scheme_Type + '50' END AS Payment_Type, dbo.Tables_Combined.SAP_CA, dbo.Tables_Combined.GeneratorID, 
                              dbo.Tables_Combined.FiT_ID_Initial, dbo.Tables_Combined.FiT_ID_Ext1, dbo.Tables_Combined.FiT_ID_Ext2, dbo.Tables_Combined.BP_ID, dbo.Tables_Combined.CONTRACT_ID, 
                              dbo.Tables_Combined.INSTALLATION_ID, dbo.Tables_Combined.Company, dbo.Tables_Combined.Title, dbo.Tables_Combined.First_Name, dbo.Tables_Combined.Last_Name, 
                              dbo.Tables_Combined.Third_Party_Ref, dbo.Tables_Combined.BP_Address_1, dbo.Tables_Combined.BP_Address_2, dbo.Tables_Combined.BP_City, dbo.Tables_Combined.BP_County, 
                              dbo.Tables_Combined.BP_Postal_Code, dbo.Tables_Combined.Initial_System_Pct_Split, dbo.Tables_Combined.Extension_1_Pct_Split, dbo.Tables_Combined.Extension_2_Pct_Split, 
                              dbo.Tables_Combined.Export_Status, dbo.Tables_Combined.ExpPct1, dbo.Tables_Combined.Initial_System_Export_Rate, Export AS [Current_Price(Export)], dbo.Tables_Combined.ExpPct2, 
                              dbo.Tables_Combined.InitSysExpRate2, InitExp2 AS [Current_Price(InitExp2)], dbo.Tables_Combined.Extension_1_Export_Rate, Export1 AS [Current_Price(Export1)], 
                              dbo.Tables_Combined.Extension_2_Export_Rate, Export2 AS [Current_Price(Export2)], dbo.LatestPaidToRead_vw.READ_ID AS PaidToREAD_ID, dbo.LatestPaidToRead_vw.Meter_Read_Reason AS LatestPaidToRead_Meter_Read_Reason, 
                              dbo.LatestPaidToRead_vw.[Export_Read(OLD)],
							  dbo.LatestPaidToRead_vw.[Generation_Read(OLD)], dbo.LatestPaidToRead_vw.[Meter_Read_Date(OLD)], 
                              dbo.Read_For_Quarter_vw.READ_ID AS LatestREAD_ID, dbo.Read_For_Quarter_vw.Meter_Read_Reason AS ReadForQuarter_Meter_Read_Reason, dbo.Read_For_Quarter_vw.[Export_Read(NEW)], 
                              dbo.Read_For_Quarter_vw.[Generation_Read(NEW)], dbo.Read_For_Quarter_vw.[Meter_Read_Date(NEW)], dbo.Tables_Combined.Initial_System_Tariff_Rate, Initial AS [Current Price(Initial)], 
                              dbo.Tables_Combined.Extension_1_Tariff_Rate, Ext1 AS [Current Price(Ext1)], dbo.Tables_Combined.Extension_2_Tariff_Rate, Ext2 AS [Current Price(Ext2)], 
                              dbo.Tables_Combined.Payment_Method, dbo.Tables_Combined.Account_Status, dbo.Tables_Combined.Move_In, dbo.Tables_Combined.Move_Out,
							 
							 
							 
							 
							 
							  CASE 
WHEN Datediff (DAY,'01Apr2010',[Meter_Read_Date(OLD)]) < 0 Then 1
WHEN Datediff (DAY,'01Apr2011',[Meter_Read_Date(OLD)]) < 0 Then 2
WHEN Datediff (DAY,'01Apr2012',[Meter_Read_Date(OLD)]) < 0 Then 3
WHEN Datediff (DAY,'01Apr2013',[Meter_Read_Date(OLD)]) < 0 Then 4
WHEN Datediff (DAY,'01Apr2014',[Meter_Read_Date(OLD)]) < 0 Then 5
WHEN Datediff (DAY,'01Apr2015',[Meter_Read_Date(OLD)]) < 0 Then 6
WHEN Datediff (DAY,'01Apr2016',[Meter_Read_Date(OLD)]) < 0 Then 7
WHEN Datediff (DAY,'01Apr2017',[Meter_Read_Date(OLD)]) < 0 Then 8
WHEN Datediff (DAY,'01Apr2018',[Meter_Read_Date(OLD)]) < 0 Then 9
WHEN Datediff (DAY,'01Apr2019',[Meter_Read_Date(OLD)]) < 0 Then 10
WHEN Datediff (DAY,'01Apr2020',[Meter_Read_Date(OLD)]) < 0 Then 11
END AS Start_Fit_Year,

CASE 
WHEN Datediff (DAY,'31Mar2010',[Meter_Read_Date(NEW)]) < 0 Then 1
WHEN Datediff (DAY,'31Mar2011',[Meter_Read_Date(NEW)]) < 0 Then 2
WHEN Datediff (DAY,'31Mar2012',[Meter_Read_Date(NEW)]) < 0 Then 3
WHEN Datediff (DAY,'31Mar2013',[Meter_Read_Date(NEW)]) < 0 Then 4
WHEN Datediff (DAY,'31Mar2014',[Meter_Read_Date(NEW)]) < 0 Then 5
WHEN Datediff (DAY,'31Mar2015',[Meter_Read_Date(NEW)]) < 0 Then 6
WHEN Datediff (DAY,'31Mar2016',[Meter_Read_Date(NEW)]) < 0 Then 7
WHEN Datediff (DAY,'31Mar2017',[Meter_Read_Date(NEW)]) < 0 Then 8
WHEN Datediff (DAY,'31Mar2018',[Meter_Read_Date(NEW)]) < 0 Then 9
WHEN Datediff (DAY,'31Mar2019',[Meter_Read_Date(NEW)]) < 0 Then 10
WHEN Datediff (DAY,'31Mar2020',[Meter_Read_Date(NEW)]) < 0 Then 11
END AS END_Fit_Year
     FROM            dbo.Tables_Combined LEFT OUTER JOIN
                              dbo.LatestPaidToRead_vw ON dbo.Tables_Combined.INSTALLATION_ID = dbo.LatestPaidToRead_vw.INSTALLATION_ID_link LEFT OUTER JOIN
                              dbo.Read_For_Quarter_vw ON dbo.Tables_Combined.CONTRACT_ID = dbo.Read_For_Quarter_vw.CONTRACT_ID 
 
      WHERE        (dbo.Read_For_Quarter_vw.[Meter_Read_Date(NEW)] IS NOT NULL) AND (dbo.Tables_Combined.Account_Status = 'Change of Supply In Progress' OR
                              dbo.Tables_Combined.Account_Status = 'Closed' OR
                              dbo.Tables_Combined.Account_Status = 'COT Complete' OR
                              dbo.Tables_Combined.Account_Status = 'Lost Customer' OR
                              dbo.Tables_Combined.Account_Status = 'Registered - COT New Occupier' OR
                              dbo.Tables_Combined.Account_Status = 'Registered Live') AND (dbo.Tables_Combined.Move_Out > dbo.LatestPaidToRead_vw.[Meter_Read_Date(OLD)])






GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Tables_Combined"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 330
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LatestPaidToRead_vw"
            Begin Extent = 
               Top = 6
               Left = 368
               Bottom = 136
               Right = 579
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Read_For_Quarter_vw"
            Begin Extent = 
               Top = 6
               Left = 617
               Bottom = 136
               Right = 831
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_1"
            Begin Extent = 
               Top = 6
               Left = 1085
               Bottom = 136
               Right = 1263
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_2"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_3"
            Begin Extent = 
               Top = 138
               Left = 254
               Bottom = 268
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_4"
            Begin Extent = 
               Top = 138
               Left = 470
               Bottom = 268
             ', 'SCHEMA', N'dbo', 'VIEW', N'Levelisation_Generator_Initial_Extract_vw', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'  Right = 648
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_6"
            Begin Extent = 
               Top = 138
               Left = 686
               Bottom = 268
               Right = 864
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_7"
            Begin Extent = 
               Top = 138
               Left = 902
               Bottom = 268
               Right = 1080
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_5"
            Begin Extent = 
               Top = 138
               Left = 1118
               Bottom = 268
               Right = 1296
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_A"
            Begin Extent = 
               Top = 6
               Left = 869
               Bottom = 136
               Right = 1047
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Levelisation_Generator_Initial_Extract_vw', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'Levelisation_Generator_Initial_Extract_vw', NULL, NULL
GO
