SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[rates_current_vw] as select * from rates where valid_to >= getdate()
GO
GRANT SELECT ON  [dbo].[rates_current_vw] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[rates_current_vw] TO [userRole]
GO
GRANT SELECT ON  [dbo].[rates_current_vw] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[rates_current_vw] TO [userRole]
GO
