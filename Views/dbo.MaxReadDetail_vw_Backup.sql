SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE view [dbo].[MaxReadDetail_vw_Backup] as
WITH a_cte as
(
	SELECT  distinct af.FiT_ID_Initial as Fit_id, rd.read_id,
	af.INSTALLATION_ID, 
	af.Generation_Type, 
	af.Generation_Capacity, 
	af.Total_Installed_Capacity_kW, 
	rd.Meter_Read_Type, 
	rd.Meter_Read_Date, 
	rd.Meter_Read_Reason, 
	rd.Generation_Read, 
	rd.Export_Read, 
	rd.IncludedInLevelisation,
	rd.Generation_clocked,
	rd.Export_clocked,
	MSN,
	rd.Recieved_Method,
	rd.Photo
	FROM installation af LEFT JOIN Readings rd ON 
		--(af.MaxOfMeter_Read_Date = rd.Meter_Read_Date) AND 
		(af.INSTALLATION_ID = rd.INSTALLATION_ID_link) 
	where 
		--af.FiT_ID_initial in ('FIT00114704-1','FIT00006978-1', 'FIT00340378-1','FIT00151437-1,','FIT00216536-1','FIT00008279-1','FIT00142176-1','FIT00151437-3') AND
	  rd.READ_ID = (select MAX(read_id) from READINGS as rd2
					  where rd2.installation_id_link = rd.INSTALLATION_ID_link )
					  
) ,
b_cte AS
(
Select a.fit_id,max(read_id) as read_id from a_cte a group by fit_id
)
select a.* from a_cte a join b_cte b ON a.read_id = b.read_id 
UNION
-- Possible to have FIT_ID with no reading.
SELECT af.FiT_ID, NULL,
af.INSTALLATION_ID, 
af.Generation_Type, 
af.Generation_Capacity, 
af.Total_Installed_Capacity_kW, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL,NULL,NULL,NULL 
FROM AllFiTwithMaxRead_vw af 
LEFT JOIN READINGS R on af.INSTALLATION_ID = r.INSTALLATION_ID_link 
WHERE r.READ_ID is null AND FiT_ID IS NOT NULL




GO
