SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Bulk_COT_Data]
AS
SELECT        dbo.Bulk_COT.FIT_ID, dbo.Bulk_COT.Third_Party_Ref, dbo.Bulk_COT.Address_1, dbo.Bulk_COT.Address_2, dbo.Bulk_COT.Postcode, dbo.Bulk_COT.Previous_Reading, dbo.Bulk_COT.New_Reading, 
                         dbo.Bulk_COT.New_Reading_Date, dbo.Bulk_COT.Old_3rd_Party, dbo.Bulk_COT.New_3rd_Party, CONTRACT_1.INSTALLATION_ID_link, dbo.Bank_Details.Sort_Code AS Old_Sort_Code, 
                         dbo.Bank_Details.Bank_Acc_Number AS Old_Bank_Account, Bank_Details_1.Sort_Code AS New_Sort_Code, Bank_Details_1.Bank_Acc_Number AS New_Bank_account, 
                         CASE WHEN Bulk_COT.Third_Party_Ref = CONTRACT_1.Third_Party_Ref THEN 'Good' ELSE 'Error' END AS Thrd_Prty_Ref_Chk, 
                         CASE WHEN Bulk_COT.Old_3rd_Party = Bank_Details.Third_Party THEN 'Good' WHEN Bulk_COT.New_3rd_Party = Bank_Details.Third_Party THEN 'Good' ELSE 'Error' END AS Thrd_Pty_Chk, 
                         CASE WHEN Bulk_COT.New_3rd_Party <> CONTRACT_1.Third_Party THEN 'Good' ELSE 'Error' END AS Thrd_Pty_Chk2, 
                         CASE WHEN Bulk_COT.Old_3rd_Party = CONTRACT_1.Third_Party THEN 'Good' ELSE 'Error' END AS Thrd_Pty_Chk3, 
                         CASE WHEN Bulk_COT.Postcode = INSTALLATION.Postal_Code THEN 'Good' ELSE 'Error' END AS PostCde_Chk, 
                         CASE WHEN Bulk_COT.Previous_Reading = derived_Readings_2.Generation_Read THEN 'Good' ELSE 'Error' END AS Reading_Chk, 
                         CASE WHEN Account_Status <> 'Registered Pending' THEN 'Good' ELSE 'Error' END AS ActSts_Chk, 
                         CASE WHEN New_Reading_Date >= derived_Contract_1.Max_Move_In THEN 'Good' ELSE 'Error' END AS MoveIn_Chk
FROM            (SELECT        INSTALLATION_ID_link, MAX(Move_In) AS Max_Move_In
                          FROM            dbo.CONTRACT
                          GROUP BY INSTALLATION_ID_link) AS derived_Contract_1 INNER JOIN
                             (SELECT DISTINCT INSTALLATION_ID_link, Generation_Read
                               FROM            dbo.READINGS) AS derived_Readings_2 INNER JOIN
                         dbo.INSTALLATION ON derived_Readings_2.INSTALLATION_ID_link = dbo.INSTALLATION.INSTALLATION_ID ON derived_Contract_1.INSTALLATION_ID_link = dbo.INSTALLATION.INSTALLATION_ID INNER JOIN
                         dbo.CONTRACT AS CONTRACT_1 ON derived_Contract_1.INSTALLATION_ID_link = CONTRACT_1.INSTALLATION_ID_link AND derived_Contract_1.Max_Move_In = CONTRACT_1.Move_In RIGHT OUTER JOIN
                         dbo.Bank_Details INNER JOIN
                         dbo.Bulk_COT ON dbo.Bank_Details.Third_Party = dbo.Bulk_COT.Old_3rd_Party INNER JOIN
                         dbo.Bank_Details AS Bank_Details_1 ON dbo.Bulk_COT.New_3rd_Party = Bank_Details_1.Third_Party ON dbo.INSTALLATION.FiT_ID_Initial = dbo.Bulk_COT.FIT_ID AND 
                         derived_Readings_2.Generation_Read = dbo.Bulk_COT.Previous_Reading
GO
GRANT SELECT ON  [dbo].[Bulk_COT_Data] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Bulk_COT_Data] TO [userRole]
GO
GRANT SELECT ON  [dbo].[Bulk_COT_Data] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[Bulk_COT_Data] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[64] 4[3] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -288
         Left = -821
      End
      Begin Tables = 
         Begin Table = "derived_Readings_2"
            Begin Extent = 
               Top = 6
               Left = 1460
               Bottom = 102
               Right = 1670
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "INSTALLATION"
            Begin Extent = 
               Top = 0
               Left = 965
               Bottom = 292
               Right = 1174
            End
            DisplayFlags = 280
            TopColumn = 25
         End
         Begin Table = "CONTRACT_1"
            Begin Extent = 
               Top = 0
               Left = 1765
               Bottom = 454
               Right = 2057
            End
            DisplayFlags = 280
            TopColumn = 35
         End
         Begin Table = "Bank_Details"
            Begin Extent = 
               Top = 135
               Left = 102
               Bottom = 296
               Right = 291
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Bulk_COT"
            Begin Extent = 
               Top = 0
               Left = 527
               Bottom = 240
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Bank_Details_1"
            Begin Extent = 
               Top = 298
               Left = 98
               Bottom = 459
               Right = 287
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "derived_Contract_1"
            Begin Extent = 
               Top = 294
               Left = 859
               Bottom = 390
               Right = ', 'SCHEMA', N'dbo', 'VIEW', N'Bulk_COT_Data', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'1069
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Bulk_COT_Data', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'Bulk_COT_Data', NULL, NULL
GO
