SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[LatestPaidToRead_vw]
AS
WITH MaxOfPaidToRead AS (SELECT        INSTALLATION_ID_link, MAX(Meter_Read_Date) AS MaxOfMeter_Read_Date, IncludedInLevelisation
                                                               FROM            dbo.READINGS
                                                               WHERE        (IncludedInLevelisation = 'True')
                                                               GROUP BY INSTALLATION_ID_link, IncludedInLevelisation)
    SELECT        READINGS_1.READ_ID, READINGS_1.INSTALLATION_ID_link, READINGS_1.Meter_Read_Type, READINGS_1.Meter_Read_Date AS [Meter_Read_Date(OLD)], READINGS_1.Meter_Read_Reason, 
                              READINGS_1.Generation_Read AS [Generation_Read(OLD)], READINGS_1.Export_Read AS [Export_Read(OLD)], READINGS_1.IncludedInLevelisation, READINGS_1.Gen_Validation, READINGS_1.Modified_By, 
                              READINGS_1.Date_Modified, READINGS_1.Time_Modified, READINGS_1.Claimed, READINGS_1.SSMA_TimeStamp
     FROM            dbo.READINGS AS READINGS_1 INNER JOIN
                              MaxOfPaidToRead AS MaxOfPaidToRead_1 ON READINGS_1.INSTALLATION_ID_link = MaxOfPaidToRead_1.INSTALLATION_ID_link AND 
                              READINGS_1.Meter_Read_Date = MaxOfPaidToRead_1.MaxOfMeter_Read_Date AND READINGS_1.IncludedInLevelisation = MaxOfPaidToRead_1.IncludedInLevelisation
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "READINGS_1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 248
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaxOfPaidToRead_1"
            Begin Extent = 
               Top = 6
               Left = 286
               Bottom = 119
               Right = 501
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'LatestPaidToRead_vw', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'LatestPaidToRead_vw', NULL, NULL
GO
