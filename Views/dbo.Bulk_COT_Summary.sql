SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Bulk_COT_Summary]
AS
SELECT        FIT_ID, 
Third_Party_Ref, 
INSTALLATION_ID_link, 
Address_1, 
Address_2, 
Postcode, 
Previous_Reading, 
New_Reading, 
New_Reading_Date, 
Old_3rd_Party, 
New_3rd_Party, 
Old_Sort_Code, 
Old_Bank_Account, 
New_Sort_Code, 
New_Bank_account, 
Thrd_Prty_Ref_Chk, 
Thrd_Pty_Chk, 
Thrd_Pty_Chk2, 
PostCde_Chk, 
Reading_Chk, 
CASE 
WHEN [Thrd_Prty_Ref_Chk] = 'Error' THEN 'Error' 
WHEN [Thrd_Pty_Chk] = 'Error' THEN 'Error' 
WHEN [Thrd_Pty_Chk2] = 'Error' THEN 'Error' 
WHEN [Thrd_Pty_Chk3] = 'Error' THEN 'Error' 
WHEN [PostCde_Chk]= 'Error' THEN 'Error' 
WHEN [Reading_Chk] = 'Error' THEN 'Error' 
WHEN [ActSts_Chk] = 'Error' THEN 'Error' 
WHEN [MoveIn_Chk] = 'Error' THEN 'Error' 
ELSE 'Good' 
END AS Check_All, 
MoveIn_Chk
FROM            dbo.Bulk_COT_Data

GO
GRANT SELECT ON  [dbo].[Bulk_COT_Summary] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[Bulk_COT_Summary] TO [userRole]
GO
GRANT SELECT ON  [dbo].[Bulk_COT_Summary] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[Bulk_COT_Summary] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Bulk_COT_Data"
            Begin Extent = 
               Top = 4
               Left = 321
               Bottom = 301
               Right = 511
            End
            DisplayFlags = 280
            TopColumn = 12
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Bulk_COT_Summary', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'Bulk_COT_Summary', NULL, NULL
GO
