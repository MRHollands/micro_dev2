SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE view [dbo].[MaxReadDetail_vw] as
WITH a_cte as
(
	SELECT distinct 
	af.FiT_ID_Initial as Fit_id, 
	rd.read_id,
	af.INSTALLATION_ID, 
	af.Generation_Type, 
	af.Generation_Capacity, 
	af.Total_Installed_Capacity_kW, 
	rd.Meter_Read_Type, 
	rd.Meter_Read_Date, 
	rd.Meter_Read_Reason, 
	rd.Generation_Read, 
	rd.Export_Read, 
	rd.IncludedInLevelisation,
	rd.Generation_clocked,
	rd.Export_clocked,
	MSN,
	rd.Recieved_Method,
	rd.Photo
	FROM installation af LEFT JOIN Readings rd ON 
		(af.INSTALLATION_ID = rd.INSTALLATION_ID_link) 
	where 
	  rd.Meter_Read_Date = (select MAX(Meter_Read_Date) from READINGS as rd2
					  where rd2.installation_id_link = rd.INSTALLATION_ID_link )
					  
) ,

b_cte AS
(
Select a.fit_id,max(Meter_Read_Date) as Maxdate, READ_ID from a_cte a group by fit_id, READ_ID
)

select a.* from a_cte a join b_cte b ON a.read_id = b.read_id 

UNION
-- Possible to have FIT_ID with no reading.
SELECT af.FiT_ID, NULL,
af.INSTALLATION_ID, 
af.Generation_Type, 
af.Generation_Capacity, 
af.Total_Installed_Capacity_kW, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL,NULL,NULL,NULL 
FROM AllFiTwithMaxRead_vw af 
LEFT JOIN READINGS R on af.INSTALLATION_ID = r.INSTALLATION_ID_link 
WHERE r.READ_ID is null AND FiT_ID IS NOT NULL




GO
GRANT SELECT ON  [dbo].[MaxReadDetail_vw] TO [admin_role]
GO
GRANT INSERT ON  [dbo].[MaxReadDetail_vw] TO [userRole]
GO
GRANT SELECT ON  [dbo].[MaxReadDetail_vw] TO [userRole]
GO
GRANT UPDATE ON  [dbo].[MaxReadDetail_vw] TO [userRole]
GO
