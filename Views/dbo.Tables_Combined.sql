SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Tables_Combined]
AS
SELECT        dbo.BP.BP_ID, dbo.BP.GeneratorID, dbo.BP.Customer_Type, dbo.BP.Company, dbo.BP.Title, dbo.BP.Last_Name, dbo.BP.First_Name, dbo.BP.VAT_Reg_Number, dbo.BP.Email_Address, dbo.BP.Business_Phone,
                          dbo.BP.Home_Phone, dbo.BP.Mobile_Phone, dbo.BP.Fax_Number, dbo.BP.Paperless_Customer, dbo.BP.Contact_Method_Email, dbo.BP.Contact_Method_Letter, dbo.BP.Address_1 AS BP_Address_1, 
                         dbo.BP.Address_2 AS BP_Address_2, dbo.BP.City AS BP_City, dbo.BP.County AS BP_County, dbo.BP.Postal_Code AS BP_Postal_Code, dbo.BP.Added_To_SAP, dbo.BP.ID_Received, dbo.BP.Modified_By, 
                         dbo.BP.Date_Modified, dbo.BP.Time_Modified, dbo.BP.Created_By, dbo.BP.Created_Date, dbo.BP.Created_Time, dbo.BP.Modified_By2, dbo.BP.Created_By2, dbo.BP.Created_Date2, dbo.BP.Modified_Date2, 
                         dbo.BP.Email_2, dbo.BP.Email_3, dbo.CONTRACT.CONTRACT_ID, dbo.CONTRACT.BP_ID_link, dbo.CONTRACT.Account_Status, dbo.CONTRACT.Payment_Method, dbo.CONTRACT.Bank_Acc_Name, 
                         dbo.CONTRACT.Sort_Code, dbo.CONTRACT.Bank_Acc_Number, dbo.CONTRACT.Bank_Reference, dbo.CONTRACT.SAP_BP, dbo.CONTRACT.SAP_CA, dbo.CONTRACT.SAP_Contract, 
                         dbo.CONTRACT.Terms_and_Conditions_Sent_Out_Date, dbo.CONTRACT.Terms_and_Conditions_Agreed_Date, dbo.CONTRACT.Terms_and_Conditions_Agreed, dbo.CONTRACT.Proof_of_Ownership_Received, 
                         dbo.CONTRACT.Date_of_Last_Reaffirmation, dbo.CONTRACT.RO_Transfer, dbo.CONTRACT.Final_bill_paid, dbo.CONTRACT.Grant_Received, dbo.CONTRACT.Grant_Form_Completed, dbo.CONTRACT.Third_Party, 
                         dbo.CONTRACT.Third_Party_Ref, dbo.CONTRACT.NR_Company, dbo.CONTRACT.NR_Title, dbo.CONTRACT.NR_First_Name, dbo.CONTRACT.NR_Last_Name, dbo.CONTRACT.NR_Business_Phone, 
                         dbo.CONTRACT.NR_Home_Phone, dbo.CONTRACT.NR_Mobile_Phone, dbo.CONTRACT.NR_Email_Address, dbo.CONTRACT.NR_Address_1, dbo.CONTRACT.NR_Address_2, dbo.CONTRACT.NR_City, 
                         dbo.CONTRACT.NR_County, dbo.CONTRACT.NR_Postal_Code, dbo.CONTRACT.INSTALLATION_ID_link, dbo.CONTRACT.Move_In, dbo.CONTRACT.Move_Out, dbo.CONTRACT.IMPORTANT, 
                         dbo.CONTRACT.Modified_By AS CONTRACT_Modified_By, dbo.CONTRACT.Date_Modified AS CONTRACT_Date_Modified, dbo.CONTRACT.Time_Modified AS CONTRACT_Time_Modified, 
                         dbo.CONTRACT.T_C_Sent_Out_Date_Ext1, dbo.CONTRACT.T_C_Agreed_Date_Ext1, dbo.CONTRACT.T_C_Sent_Out_Date_Ext2, dbo.CONTRACT.T_C_Agreed_Date_Ext2, dbo.CONTRACT.MultiSiteNR, 
                         dbo.CONTRACT.Vat_Inv_Rqd, dbo.CONTRACT.Created_By AS CONTRACT_Created_By, dbo.CONTRACT.Created_Date AS CONTRACT_Created_Date, dbo.CONTRACT.Created_Time AS CONTRACT_Created_Time, 
                         dbo.CONTRACT.modified_by2 AS CONTRACT_modified_by2, dbo.CONTRACT.created_by2 AS CONTRACT_created_by2, dbo.CONTRACT.created_date2 AS CONTRACT_created_date2, 
                         dbo.CONTRACT.modified_date2 AS CONTRACT_modified_date2, dbo.CONTRACT.Postcoder_Errors, dbo.CONTRACT.Microtricity_Errors, dbo.INSTALLATION.INSTALLATION_ID, dbo.INSTALLATION.Scheme_Type, 
                         dbo.INSTALLATION.Supply_MPAN, dbo.INSTALLATION.Export_MPAN, dbo.INSTALLATION.Export_Status, dbo.INSTALLATION.Address_1 AS INSTALLATION_Address_1, 
                         dbo.INSTALLATION.Address_2 AS INSTALLATION_Address_2, dbo.INSTALLATION.City AS INSTALLATION_City, dbo.INSTALLATION.County AS INSTALLATION_County, 
                         dbo.INSTALLATION.Postal_Code AS INSTALLATION_Postal_Code, dbo.INSTALLATION.OS_Grid_Ref, dbo.INSTALLATION.Type_of_Installation, dbo.INSTALLATION.Generation_Type, 
                         dbo.INSTALLATION.Generation_Capacity, dbo.INSTALLATION.Generation_MSN, dbo.INSTALLATION.Export_MSN, dbo.INSTALLATION.Total_Installed_Capacity_kW, 
                         dbo.INSTALLATION.Total_Declared_Net_Capacity_kW, dbo.INSTALLATION.Estimated_Annual_Generation, dbo.INSTALLATION.RO_Accreditation_Number, dbo.INSTALLATION.Make_of_Generation_Equipment, 
                         dbo.INSTALLATION.Model_of_Generation_Equipment, dbo.INSTALLATION.Installation_Name, dbo.INSTALLATION.Generation_Meter_Make_and_Model, dbo.INSTALLATION.Agent_Appointment_Letter_Received, 
                         dbo.INSTALLATION.LOA_Received, dbo.INSTALLATION.Electrical_Schematic_Received, dbo.INSTALLATION.EPC_Reference, dbo.INSTALLATION.Application_Received_Date, dbo.INSTALLATION.FiT_ID_Initial, 
                         dbo.INSTALLATION.MCS_Certification_Number_1, dbo.INSTALLATION.Initial_System_Tariff_Rate, dbo.INSTALLATION.Initial_System_Export_Rate, dbo.INSTALLATION.Initial_System_Pct_Split, 
                         dbo.INSTALLATION.Initial_System_Start_Date, dbo.INSTALLATION.Initial_System_End_Date, dbo.INSTALLATION.Initial_System_Commissioning_Date, dbo.INSTALLATION.FiT_ID_Ext1, 
                         dbo.INSTALLATION.MCS_Certification_Number_2, dbo.INSTALLATION.Extension_1_Tariff_Rate, dbo.INSTALLATION.Extension_1_Export_Rate, dbo.INSTALLATION.Extension_1_Pct_Split, 
                         dbo.INSTALLATION.Extension_1_Start_Date, dbo.INSTALLATION.Extension_1_End_Date, dbo.INSTALLATION.Extension_1_Commissioning_Date, dbo.INSTALLATION.FiT_ID_Ext2, 
                         dbo.INSTALLATION.MCS_Certification_Number_3, dbo.INSTALLATION.Extension_2_Tariff_Rate, dbo.INSTALLATION.Extension_2_Export_Rate, dbo.INSTALLATION.Extension_2_Pct_Split, 
                         dbo.INSTALLATION.Extension_2_Start_Date, dbo.INSTALLATION.Extension_2_End_Date, dbo.INSTALLATION.Extension_2_Commissioning_Date, dbo.INSTALLATION.TopUp_Tariff_Rate, 
                         dbo.INSTALLATION.Modified_By AS INSTALLATION_Modified_By, dbo.INSTALLATION.Date_Modified AS INSTALLATION_Date_Modified, dbo.INSTALLATION.Time_Modified AS INSTALLATION_Time_Modified, 
                         dbo.INSTALLATION.Cos_Flag, dbo.INSTALLATION.SysDesc, dbo.INSTALLATION.ExpPct1, dbo.INSTALLATION.ExpPct2, dbo.INSTALLATION.InitSysExpRate2, dbo.INSTALLATION.FiT_Related, dbo.INSTALLATION.DC, 
                         dbo.INSTALLATION.Created_By AS INSTALLATION_Created_By, dbo.INSTALLATION.Created_Date AS INSTALLATION_Created_Date, dbo.INSTALLATION.Created_Time AS INSTALLATION_Created_Time, 
                         dbo.INSTALLATION.Export_Meter_Photo, dbo.INSTALLATION.Confirmation_Date, dbo.INSTALLATION.Ext1_Confirmation_Date, dbo.INSTALLATION.Ext2_Confirmation_Date, 
                         dbo.INSTALLATION.Modified_by2 AS INSTALLATION_Modified_by2, dbo.INSTALLATION.Created_by2 AS INSTALLATION_Created_by2, dbo.INSTALLATION.Created_date2 AS INSTALLATION_Created_date2, 
                         dbo.INSTALLATION.Modified_date2 AS INSTALLATION_Modified_date2, dbo.INSTALLATION.Remotely_Read_Enabled_From, dbo.INSTALLATION.Remotely_Read_Enabled, dbo.INSTALLATION.Epc_Date, 
                         dbo.INSTALLATION.Valid_From, dbo.INSTALLATION.Initial_Meter_Reading, dbo.INSTALLATION.Initial_Meter_Read_Date, dbo.INSTALLATION.Originating_Client_Reference, 
                         dbo.INSTALLATION.Generation_Meter_Model, dbo.BP.Title + ' ' + dbo.BP.First_Name + ' ' + dbo.BP.Last_Name AS Full_Name, dbo.INSTALLATION.FiT_ID_Initial AS FiT_Main, 
                         Current_Price.Valid_From AS Rates_Valid_From, Current_Price.Valid_To AS Rates_Valid_To, Current_Price.Price AS Initial, Current_Price_3.Price AS Export, Current_Price_7.Price AS InitExp2, 
                         Current_Price_1.Price AS Ext1, Current_Price_2.Price AS Ext2, Current_Price_5.Price AS Export1, Current_Price_6.Price AS Export2, Current_Price_4.Price AS Topup, CASE WHEN Initial_System_Pct_Split IS NULL 
                         THEN [Total_Installed_Capacity_kW] ELSE [Initial_System_Pct_Split] / 1 * [Total_Installed_Capacity_kW] END AS Initial_System_Capacity, 
                         dbo.INSTALLATION.Extension_1_Pct_Split / 1 * dbo.INSTALLATION.Total_Installed_Capacity_kW AS Extension_1_Capacity, 
                         dbo.INSTALLATION.Extension_2_Pct_Split / 1 * dbo.INSTALLATION.Total_Installed_Capacity_kW AS Extension_2_Capacity, dbo.INSTALLATION.MCS_Issue_Date, dbo.INSTALLATION.MCS_Issue_Time, 
                         dbo.INSTALLATION.COT_Flag
FROM            dbo.BP RIGHT OUTER JOIN
                         dbo.CONTRACT ON dbo.BP.BP_ID = dbo.CONTRACT.BP_ID_link RIGHT OUTER JOIN
                         dbo.INSTALLATION ON dbo.CONTRACT.INSTALLATION_ID_link = dbo.INSTALLATION.INSTALLATION_ID LEFT OUTER JOIN
                         dbo.RATES AS Current_Price ON dbo.INSTALLATION.Initial_System_Tariff_Rate = Current_Price.Tariff_Code AND Current_Price.Valid_From <= GETDATE() AND Current_Price.Valid_To >= GETDATE() 
                         LEFT OUTER JOIN
                         dbo.RATES AS Current_Price_1 ON dbo.INSTALLATION.Extension_1_Tariff_Rate = Current_Price_1.Tariff_Code AND Current_Price_1.Valid_From <= GETDATE() AND Current_Price_1.Valid_To >= GETDATE() 
                         LEFT OUTER JOIN
                         dbo.RATES AS Current_Price_2 ON dbo.INSTALLATION.Extension_2_Tariff_Rate = Current_Price_2.Tariff_Code AND Current_Price_2.Valid_From <= GETDATE() AND Current_Price_2.Valid_To >= GETDATE() 
                         LEFT OUTER JOIN
                         dbo.RATES AS Current_Price_3 ON dbo.INSTALLATION.Initial_System_Export_Rate = Current_Price_3.Tariff_Code AND Current_Price_3.Valid_From <= GETDATE() AND Current_Price_3.Valid_To >= GETDATE() 
                         LEFT OUTER JOIN
                         dbo.RATES AS Current_Price_4 ON dbo.INSTALLATION.TopUp_Tariff_Rate = Current_Price_4.Tariff_Code AND Current_Price_4.Valid_From <= GETDATE() AND Current_Price_4.Valid_To >= GETDATE() 
                         LEFT OUTER JOIN
                         dbo.RATES AS Current_Price_5 ON dbo.INSTALLATION.Extension_1_Export_Rate = Current_Price_5.Tariff_Code AND Current_Price_5.Valid_From <= GETDATE() AND Current_Price_5.Valid_To >= GETDATE() 
                         LEFT OUTER JOIN
                         dbo.RATES AS Current_Price_6 ON dbo.INSTALLATION.Extension_2_Export_Rate = Current_Price_6.Tariff_Code AND Current_Price_6.Valid_From <= GETDATE() AND Current_Price_6.Valid_To >= GETDATE() 
                         LEFT OUTER JOIN
                         dbo.RATES AS Current_Price_7 ON dbo.INSTALLATION.InitSysExpRate2 = Current_Price_7.Tariff_Code AND Current_Price_7.Valid_From <= GETDATE() AND Current_Price_7.Valid_To >= GETDATE()
GO
GRANT SELECT ON  [dbo].[Tables_Combined] TO [userRole]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BP"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CONTRACT"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 330
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "INSTALLATION"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_1"
            Begin Extent = 
               Top = 402
               Left = 265
               Bottom = 532
               Right = 454
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_2"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 664
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_3"
            Begin Extent = 
               Top = 534
               Left = 265
               Bottom = 664
               Right = 454
            End
', 'SCHEMA', N'dbo', 'VIEW', N'Tables_Combined', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_4"
            Begin Extent = 
               Top = 666
               Left = 38
               Bottom = 796
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_5"
            Begin Extent = 
               Top = 666
               Left = 265
               Bottom = 796
               Right = 454
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_6"
            Begin Extent = 
               Top = 798
               Left = 38
               Bottom = 928
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Current_Price_7"
            Begin Extent = 
               Top = 798
               Left = 265
               Bottom = 928
               Right = 454
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Tables_Combined', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'Tables_Combined', NULL, NULL
GO
