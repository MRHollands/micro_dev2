EXEC sp_addrolemember N'db_datareader', N'ECO\andrew.wakely'
GO
EXEC sp_addrolemember N'db_datareader', N'ECO\beth.wilson'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Craig.Wilkins'
GO
EXEC sp_addrolemember N'db_datareader', N'ECO\ECO-SVC-MattHollands'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Eve.Uzzell'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\George.Fensome'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Georgie.Oakes'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Hannah.Hurford'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Helen.Verey'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\james.tubb'
GO
EXEC sp_addrolemember N'db_datareader', N'ECO\jodie.humphries'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\joshua.moran'
GO
EXEC sp_addrolemember N'db_datareader', N'ECO\kara.jefferies'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\matthew.hollands'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Nathan.Buckland'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Niall.Kimber'
GO
EXEC sp_addrolemember N'db_datareader', N'ECO\SallyAnne.Beer'
GO
EXEC sp_addrolemember N'db_datareader', N'ECO\sapphire.mahony'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Shahid.Siddique'
GO
EXEC sp_addrolemember N'db_datareader', N'eco\Tonia.Williams'
GO
EXEC sp_addrolemember N'db_datareader', N'jitterbit_test'
GO
