CREATE ROLE [admin_role]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'admin_role', N'eco\alex.booth'
GO
EXEC sp_addrolemember N'admin_role', N'ECO\dan.barker'
GO
EXEC sp_addrolemember N'admin_role', N'ECO\dave.wood'
GO
EXEC sp_addrolemember N'admin_role', N'eco\glynn.hammond'
GO
EXEC sp_addrolemember N'admin_role', N'ECO\steve.lloyd'
GO
EXEC sp_addrolemember N'admin_role', N'eco\Steve.Robertson'
GO
