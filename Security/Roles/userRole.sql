CREATE ROLE [userRole]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'userRole', N'ECO\dan.barker'
GO
EXEC sp_addrolemember N'userRole', N'ECO\dave.wood'
GO
EXEC sp_addrolemember N'userRole', N'ECO\eco-loc-sql-microtricity'
GO
EXEC sp_addrolemember N'userRole', N'ECO\ECO-SVC-MattHollands'
GO
