IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Mikaela.Sherwood')
CREATE LOGIN [ECO\Mikaela.Sherwood] FROM WINDOWS
GO
CREATE USER [eco\Mikaela.Sherwood] FOR LOGIN [ECO\Mikaela.Sherwood]
GO
