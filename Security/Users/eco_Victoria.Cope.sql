IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Victoria.Cope')
CREATE LOGIN [ECO\Victoria.Cope] FROM WINDOWS
GO
CREATE USER [eco\Victoria.Cope] FOR LOGIN [ECO\Victoria.Cope]
GO
