IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Hannah.Hurford')
CREATE LOGIN [ECO\Hannah.Hurford] FROM WINDOWS
GO
CREATE USER [eco\Hannah.Hurford] FOR LOGIN [ECO\Hannah.Hurford]
GO
