IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Ingrid.Valentova')
CREATE LOGIN [ECO\Ingrid.Valentova] FROM WINDOWS
GO
CREATE USER [eco\Ingrid.Valentova] FOR LOGIN [ECO\Ingrid.Valentova]
GO
