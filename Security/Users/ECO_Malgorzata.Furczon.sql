IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Malgorzata.Furczon')
CREATE LOGIN [ECO\Malgorzata.Furczon] FROM WINDOWS
GO
CREATE USER [ECO\Malgorzata.Furczon] FOR LOGIN [ECO\Malgorzata.Furczon]
GO
