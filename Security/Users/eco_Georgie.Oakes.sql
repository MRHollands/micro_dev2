IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Georgie.Oakes')
CREATE LOGIN [ECO\Georgie.Oakes] FROM WINDOWS
GO
CREATE USER [eco\Georgie.Oakes] FOR LOGIN [ECO\Georgie.Oakes]
GO
