IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Lois.Lewis')
CREATE LOGIN [ECO\Lois.Lewis] FROM WINDOWS
GO
CREATE USER [eco\Lois.Lewis] FOR LOGIN [ECO\Lois.Lewis]
GO
