IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Stephen.Price')
CREATE LOGIN [ECO\Stephen.Price] FROM WINDOWS
GO
CREATE USER [eco\Stephen.Price] FOR LOGIN [ECO\Stephen.Price]
GO
