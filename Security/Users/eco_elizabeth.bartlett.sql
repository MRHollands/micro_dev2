IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Elizabeth.Bartlett')
CREATE LOGIN [ECO\Elizabeth.Bartlett] FROM WINDOWS
GO
CREATE USER [eco\elizabeth.bartlett] FOR LOGIN [ECO\Elizabeth.Bartlett]
GO
