IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Matthew.Hollands')
CREATE LOGIN [ECO\Matthew.Hollands] FROM WINDOWS
GO
CREATE USER [eco\matthew.hollands] FOR LOGIN [ECO\Matthew.Hollands]
GO
GRANT VIEW DEFINITION TO [eco\matthew.hollands]
