IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\James.Tubb')
CREATE LOGIN [ECO\James.Tubb] FROM WINDOWS
GO
CREATE USER [eco\james.tubb] FOR LOGIN [ECO\James.Tubb]
GO
