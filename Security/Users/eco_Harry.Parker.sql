IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Harry.Parker')
CREATE LOGIN [ECO\Harry.Parker] FROM WINDOWS
GO
CREATE USER [eco\Harry.Parker] FOR LOGIN [ECO\Harry.Parker]
GO
