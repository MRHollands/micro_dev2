IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\James.Middlecote')
CREATE LOGIN [ECO\James.Middlecote] FROM WINDOWS
GO
CREATE USER [ECO\James.Middlecote] FOR LOGIN [ECO\James.Middlecote]
GO
