IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Helen.Verey')
CREATE LOGIN [ECO\Helen.Verey] FROM WINDOWS
GO
CREATE USER [eco\Helen.Verey] FOR LOGIN [ECO\Helen.Verey]
GO
