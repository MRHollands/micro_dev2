IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Hollie.Jones')
CREATE LOGIN [ECO\Hollie.Jones] FROM WINDOWS
GO
CREATE USER [ECO\Hollie.Jones] FOR LOGIN [ECO\Hollie.Jones]
GO
