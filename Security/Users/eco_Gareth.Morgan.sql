IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Gareth.Morgan')
CREATE LOGIN [ECO\Gareth.Morgan] FROM WINDOWS
GO
CREATE USER [eco\Gareth.Morgan] FOR LOGIN [ECO\Gareth.Morgan]
GO
