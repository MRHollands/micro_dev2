IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Charlie.Bird')
CREATE LOGIN [ECO\Charlie.Bird] FROM WINDOWS
GO
CREATE USER [eco\Charlie.Bird] FOR LOGIN [ECO\Charlie.Bird]
GO
