IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Chloe.Edwards')
CREATE LOGIN [ECO\Chloe.Edwards] FROM WINDOWS
GO
CREATE USER [eco\Chloe.Edwards] FOR LOGIN [ECO\Chloe.Edwards]
GO
