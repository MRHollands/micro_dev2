IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\clive.edgington')
CREATE LOGIN [ECO\clive.edgington] FROM WINDOWS
GO
CREATE USER [eco\clive.edgington] FOR LOGIN [ECO\clive.edgington]
GO
