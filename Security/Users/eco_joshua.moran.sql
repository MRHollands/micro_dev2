IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Joshua.Moran')
CREATE LOGIN [ECO\Joshua.Moran] FROM WINDOWS
GO
CREATE USER [eco\joshua.moran] FOR LOGIN [ECO\Joshua.Moran]
GO
