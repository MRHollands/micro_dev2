IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Matthew.Boyle')
CREATE LOGIN [ECO\Matthew.Boyle] FROM WINDOWS
GO
CREATE USER [eco\Matthew.Boyle] FOR LOGIN [ECO\Matthew.Boyle]
GO
