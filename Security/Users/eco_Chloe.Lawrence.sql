IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Chloe.Lawrence')
CREATE LOGIN [ECO\Chloe.Lawrence] FROM WINDOWS
GO
CREATE USER [eco\Chloe.Lawrence] FOR LOGIN [ECO\Chloe.Lawrence]
GO
