IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Emma.Poole')
CREATE LOGIN [ECO\Emma.Poole] FROM WINDOWS
GO
CREATE USER [ECO\Emma.Poole] FOR LOGIN [ECO\Emma.Poole]
GO
