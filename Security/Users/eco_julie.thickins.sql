IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Julie.Thickins')
CREATE LOGIN [ECO\Julie.Thickins] FROM WINDOWS
GO
CREATE USER [eco\julie.thickins] FOR LOGIN [ECO\Julie.Thickins]
GO
