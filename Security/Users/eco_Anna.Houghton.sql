IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Anna.Houghton')
CREATE LOGIN [ECO\Anna.Houghton] FROM WINDOWS
GO
CREATE USER [eco\Anna.Houghton] FOR LOGIN [ECO\Anna.Houghton]
GO
