IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Harriet.Mclaughlin')
CREATE LOGIN [ECO\Harriet.Mclaughlin] FROM WINDOWS
GO
CREATE USER [ECO\Harriet.Mclaughlin] FOR LOGIN [ECO\Harriet.Mclaughlin]
GO
