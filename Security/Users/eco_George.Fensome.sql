IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'eco\George.Fensome')
CREATE LOGIN [eco\George.Fensome] FROM WINDOWS
GO
CREATE USER [eco\George.Fensome] FOR LOGIN [eco\George.Fensome]
GO
