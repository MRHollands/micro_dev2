IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\SallyAnne.Beer')
CREATE LOGIN [ECO\SallyAnne.Beer] FROM WINDOWS
GO
CREATE USER [ECO\SallyAnne.Beer] FOR LOGIN [ECO\SallyAnne.Beer]
GO
