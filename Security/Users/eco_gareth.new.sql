IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Gareth.New')
CREATE LOGIN [ECO\Gareth.New] FROM WINDOWS
GO
CREATE USER [eco\gareth.new] FOR LOGIN [ECO\Gareth.New]
GO
