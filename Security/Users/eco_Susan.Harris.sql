IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Susan.Harris')
CREATE LOGIN [ECO\Susan.Harris] FROM WINDOWS
GO
CREATE USER [eco\Susan.Harris] FOR LOGIN [ECO\Susan.Harris]
GO
