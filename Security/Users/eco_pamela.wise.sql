IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Pamela.Wise')
CREATE LOGIN [ECO\Pamela.Wise] FROM WINDOWS
GO
CREATE USER [eco\pamela.wise] FOR LOGIN [ECO\Pamela.Wise]
GO
