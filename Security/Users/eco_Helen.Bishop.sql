IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Helen.Bishop')
CREATE LOGIN [ECO\Helen.Bishop] FROM WINDOWS
GO
CREATE USER [eco\Helen.Bishop] FOR LOGIN [ECO\Helen.Bishop]
GO
