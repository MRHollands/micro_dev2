IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Chloe.Lynn')
CREATE LOGIN [ECO\Chloe.Lynn] FROM WINDOWS
GO
CREATE USER [eco\chloe.lynn] FOR LOGIN [ECO\Chloe.Lynn]
GO
