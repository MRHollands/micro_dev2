IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Nathan.Buckland')
CREATE LOGIN [ECO\Nathan.Buckland] FROM WINDOWS
GO
CREATE USER [eco\Nathan.Buckland] FOR LOGIN [ECO\Nathan.Buckland]
GO
