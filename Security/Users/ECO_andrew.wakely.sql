IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\andrew.wakely')
CREATE LOGIN [ECO\andrew.wakely] FROM WINDOWS
GO
CREATE USER [ECO\andrew.wakely] FOR LOGIN [ECO\andrew.wakely]
GO
