IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Fiona.Gordon')
CREATE LOGIN [ECO\Fiona.Gordon] FROM WINDOWS
GO
CREATE USER [eco\Fiona.Gordon] FOR LOGIN [ECO\Fiona.Gordon]
GO
