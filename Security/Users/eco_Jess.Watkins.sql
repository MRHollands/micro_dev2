IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Jess.Watkins')
CREATE LOGIN [ECO\Jess.Watkins] FROM WINDOWS
GO
CREATE USER [eco\Jess.Watkins] FOR LOGIN [ECO\Jess.Watkins]
GO
