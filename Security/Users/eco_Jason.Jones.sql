IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Jason.Jones')
CREATE LOGIN [ECO\Jason.Jones] FROM WINDOWS
GO
CREATE USER [eco\Jason.Jones] FOR LOGIN [ECO\Jason.Jones]
GO
