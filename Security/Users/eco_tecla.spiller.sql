IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Tecla.Spiller')
CREATE LOGIN [ECO\Tecla.Spiller] FROM WINDOWS
GO
CREATE USER [eco\tecla.spiller] FOR LOGIN [ECO\Tecla.Spiller]
GO
