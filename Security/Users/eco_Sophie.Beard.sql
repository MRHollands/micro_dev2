IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Sophie.Beard')
CREATE LOGIN [ECO\Sophie.Beard] FROM WINDOWS
GO
CREATE USER [eco\Sophie.Beard] FOR LOGIN [ECO\Sophie.Beard]
GO
