IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Paula.Grant')
CREATE LOGIN [ECO\Paula.Grant] FROM WINDOWS
GO
CREATE USER [eco\Paula.Grant] FOR LOGIN [ECO\Paula.Grant]
GO
