IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\craig.wilkins')
CREATE LOGIN [ECO\craig.wilkins] FROM WINDOWS
GO
CREATE USER [eco\Craig.Wilkins] FOR LOGIN [ECO\craig.wilkins] WITH DEFAULT_SCHEMA=[eco\Craig.Wilkins]
GO
