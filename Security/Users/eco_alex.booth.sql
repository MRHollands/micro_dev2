IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\alex.booth')
CREATE LOGIN [ECO\alex.booth] FROM WINDOWS
GO
CREATE USER [eco\alex.booth] FOR LOGIN [ECO\alex.booth]
GO
