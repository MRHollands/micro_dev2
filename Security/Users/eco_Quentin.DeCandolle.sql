IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Quentin.DeCandolle')
CREATE LOGIN [ECO\Quentin.DeCandolle] FROM WINDOWS
GO
CREATE USER [eco\Quentin.DeCandolle] FOR LOGIN [ECO\Quentin.DeCandolle]
GO
