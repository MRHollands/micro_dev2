IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Meghan.Jackson')
CREATE LOGIN [ECO\Meghan.Jackson] FROM WINDOWS
GO
CREATE USER [eco\Meghan.Jackson] FOR LOGIN [ECO\Meghan.Jackson]
GO
