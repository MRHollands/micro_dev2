IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\eco-loc-sql-microtricity')
CREATE LOGIN [ECO\eco-loc-sql-microtricity] FROM WINDOWS
GO
CREATE USER [ECO\eco-loc-sql-microtricity] FOR LOGIN [ECO\eco-loc-sql-microtricity]
GO
