IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Tonia.Williams')
CREATE LOGIN [ECO\Tonia.Williams] FROM WINDOWS
GO
CREATE USER [eco\Tonia.Williams] FOR LOGIN [ECO\Tonia.Williams]
GO
