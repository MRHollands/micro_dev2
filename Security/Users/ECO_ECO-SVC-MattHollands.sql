IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\ECO-SVC-MattHollands')
CREATE LOGIN [ECO\ECO-SVC-MattHollands] FROM WINDOWS
GO
CREATE USER [ECO\ECO-SVC-MattHollands] FOR LOGIN [ECO\ECO-SVC-MattHollands]
GO
