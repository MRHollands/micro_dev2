IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\dan.barker')
CREATE LOGIN [ECO\dan.barker] FROM WINDOWS
GO
CREATE USER [ECO\dan.barker] FOR LOGIN [ECO\dan.barker]
GO
