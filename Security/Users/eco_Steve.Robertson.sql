IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Steve.Robertson')
CREATE LOGIN [ECO\Steve.Robertson] FROM WINDOWS
GO
CREATE USER [eco\Steve.Robertson] FOR LOGIN [ECO\Steve.Robertson]
GO
