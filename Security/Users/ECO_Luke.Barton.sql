IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Luke.Barton')
CREATE LOGIN [ECO\Luke.Barton] FROM WINDOWS
GO
CREATE USER [ECO\Luke.Barton] FOR LOGIN [ECO\Luke.Barton]
GO
