IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Owen.Pearson')
CREATE LOGIN [ECO\Owen.Pearson] FROM WINDOWS
GO
CREATE USER [eco\Owen.Pearson] FOR LOGIN [ECO\Owen.Pearson]
GO
