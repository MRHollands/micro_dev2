IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Natalie.Reed')
CREATE LOGIN [ECO\Natalie.Reed] FROM WINDOWS
GO
CREATE USER [eco\Natalie.Reed] FOR LOGIN [ECO\Natalie.Reed]
GO
