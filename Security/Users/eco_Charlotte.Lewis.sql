IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Charlotte.Lewis')
CREATE LOGIN [ECO\Charlotte.Lewis] FROM WINDOWS
GO
CREATE USER [eco\Charlotte.Lewis] FOR LOGIN [ECO\Charlotte.Lewis]
GO
