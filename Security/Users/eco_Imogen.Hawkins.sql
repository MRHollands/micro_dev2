IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\Imogen.Hawkins')
CREATE LOGIN [ECO\Imogen.Hawkins] FROM WINDOWS
GO
CREATE USER [eco\Imogen.Hawkins] FOR LOGIN [ECO\Imogen.Hawkins]
GO
