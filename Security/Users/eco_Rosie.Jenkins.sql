IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ECO\rosie.jenkins')
CREATE LOGIN [ECO\rosie.jenkins] FROM WINDOWS
GO
CREATE USER [eco\Rosie.Jenkins] FOR LOGIN [ECO\rosie.jenkins]
GO
